/*Copyright (c) 2016-2017 curaguard.co.uk All Rights Reserved.
 This software is the confidential and proprietary information of curaguard.co.uk You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with curaguard.co.uk*/

package com.curaguard_customer_portal.banddata;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

public class UserAlertsId implements Serializable {

    private int userId;
    private int alertId;

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAlertId() {
        return this.alertId;
    }

    public void setAlertId(int alertId) {
        this.alertId = alertId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAlerts)) return false;
        final UserAlerts userAlerts = (UserAlerts) o;
        return Objects.equals(getUserId(), userAlerts.getUserId()) &&
                Objects.equals(getAlertId(), userAlerts.getAlertId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(),
                getAlertId());
    }
}
