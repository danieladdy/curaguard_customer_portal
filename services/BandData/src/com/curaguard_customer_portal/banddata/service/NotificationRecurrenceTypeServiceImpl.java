/*Copyright (c) 2016-2017 curaguard.co.uk All Rights Reserved.
 This software is the confidential and proprietary information of curaguard.co.uk You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with curaguard.co.uk*/

package com.curaguard_customer_portal.banddata.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.curaguard_customer_portal.banddata.NotificationRecurrenceType;

/**
 * ServiceImpl object for domain model class NotificationRecurrenceType.
 *
 * @see NotificationRecurrenceType
 */
@Service("BandData.NotificationRecurrenceTypeService")
public class NotificationRecurrenceTypeServiceImpl implements NotificationRecurrenceTypeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationRecurrenceTypeServiceImpl.class);

    @Autowired
    @Qualifier("BandData.NotificationRecurrenceTypeDao")
    private WMGenericDao<NotificationRecurrenceType, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<NotificationRecurrenceType, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "BandDataTransactionManager")
    @Override
	public NotificationRecurrenceType create(NotificationRecurrenceType notificationrecurrencetype) {
        LOGGER.debug("Creating a new NotificationRecurrenceType with information: {}", notificationrecurrencetype);
        return this.wmGenericDao.create(notificationrecurrencetype);
    }

	@Transactional(readOnly = true, value = "BandDataTransactionManager")
	@Override
	public NotificationRecurrenceType getById(Integer notificationrecurrencetypeId) throws EntityNotFoundException {
        LOGGER.debug("Finding NotificationRecurrenceType by id: {}", notificationrecurrencetypeId);
        NotificationRecurrenceType notificationrecurrencetype = this.wmGenericDao.findById(notificationrecurrencetypeId);
        if (notificationrecurrencetype == null){
            LOGGER.debug("No NotificationRecurrenceType found with id: {}", notificationrecurrencetypeId);
            throw new EntityNotFoundException(String.valueOf(notificationrecurrencetypeId));
        }
        return notificationrecurrencetype;
    }

	@Transactional(rollbackFor = EntityNotFoundException.class, value = "BandDataTransactionManager")
	@Override
	public NotificationRecurrenceType update(NotificationRecurrenceType notificationrecurrencetype) throws EntityNotFoundException {
        LOGGER.debug("Updating NotificationRecurrenceType with information: {}", notificationrecurrencetype);
        this.wmGenericDao.update(notificationrecurrencetype);

        Integer notificationrecurrencetypeId = notificationrecurrencetype.getRecurrenceTypeId();

        return this.wmGenericDao.findById(notificationrecurrencetypeId);
    }

    @Transactional(value = "BandDataTransactionManager")
	@Override
	public NotificationRecurrenceType delete(Integer notificationrecurrencetypeId) throws EntityNotFoundException {
        LOGGER.debug("Deleting NotificationRecurrenceType with id: {}", notificationrecurrencetypeId);
        NotificationRecurrenceType deleted = this.wmGenericDao.findById(notificationrecurrencetypeId);
        if (deleted == null) {
            LOGGER.debug("No NotificationRecurrenceType found with id: {}", notificationrecurrencetypeId);
            throw new EntityNotFoundException(String.valueOf(notificationrecurrencetypeId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "BandDataTransactionManager")
	@Override
	public Page<NotificationRecurrenceType> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all NotificationRecurrenceTypes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "BandDataTransactionManager")
    @Override
    public Page<NotificationRecurrenceType> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all NotificationRecurrenceTypes");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "BandDataTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service BandData for table NotificationRecurrenceType to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "BandDataTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "BandDataTransactionManager")
    @SuppressWarnings("unchecked")
	@Override
    public Page<NotificationRecurrenceType> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable) {
        LOGGER.debug("Fetching all associated");
        return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
    }
}

