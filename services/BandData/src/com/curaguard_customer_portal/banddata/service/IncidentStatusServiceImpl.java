/*Copyright (c) 2016-2017 curaguard.co.uk All Rights Reserved.
 This software is the confidential and proprietary information of curaguard.co.uk You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with curaguard.co.uk*/

package com.curaguard_customer_portal.banddata.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.curaguard_customer_portal.banddata.IncidentStatus;

/**
 * ServiceImpl object for domain model class IncidentStatus.
 *
 * @see IncidentStatus
 */
@Service("BandData.IncidentStatusService")
public class IncidentStatusServiceImpl implements IncidentStatusService {

    private static final Logger LOGGER = LoggerFactory.getLogger(IncidentStatusServiceImpl.class);

    @Autowired
    @Qualifier("BandData.IncidentStatusDao")
    private WMGenericDao<IncidentStatus, String> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<IncidentStatus, String> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

	@Transactional(readOnly = true, value = "BandDataTransactionManager")
	@Override
	public IncidentStatus getById(String incidentstatusId) throws EntityNotFoundException {
        LOGGER.debug("Finding IncidentStatus by id: {}", incidentstatusId);
        IncidentStatus incidentstatus = this.wmGenericDao.findById(incidentstatusId);
        if (incidentstatus == null){
            LOGGER.debug("No IncidentStatus found with id: {}", incidentstatusId);
            throw new EntityNotFoundException(String.valueOf(incidentstatusId));
        }
        return incidentstatus;
    }

	@Transactional(readOnly = true, value = "BandDataTransactionManager")
	@Override
	public Page<IncidentStatus> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all IncidentStatuses");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "BandDataTransactionManager")
    @Override
    public Page<IncidentStatus> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all IncidentStatuses");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "BandDataTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service BandData for table IncidentStatus to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "BandDataTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "BandDataTransactionManager")
    @SuppressWarnings("unchecked")
	@Override
    public Page<IncidentStatus> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable) {
        LOGGER.debug("Fetching all associated");
        return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
    }
}

