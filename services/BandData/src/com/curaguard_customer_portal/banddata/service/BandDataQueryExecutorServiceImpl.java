/*Copyright (c) 2016-2017 curaguard.co.uk All Rights Reserved.
 This software is the confidential and proprietary information of curaguard.co.uk You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with curaguard.co.uk*/


package com.curaguard_customer_portal.banddata.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.HashMap;
import java.util.Map;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.wavemaker.runtime.data.model.CustomQuery;
import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.exception.QueryParameterMismatchException;

@Service
public class BandDataQueryExecutorServiceImpl implements BandDataQueryExecutorService {
	private static final Logger LOGGER = LoggerFactory.getLogger(BandDataQueryExecutorServiceImpl.class);

	@Autowired
	@Qualifier("BandDataWMQueryExecutor")
	private WMQueryExecutor queryExecutor;

	@Transactional(value = "BandDataTransactionManager")
	@Override
	public Page<Object> executeTelemetryByMeasure(Pageable pageable, java.lang.String StartDate, java.lang.String EndDate, java.lang.String DeviceId, java.lang.String Measure)
	throws QueryParameterMismatchException{
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("StartDate", StartDate);
        params.put("EndDate", EndDate);
        params.put("DeviceId", DeviceId);
        params.put("Measure", Measure);
        return queryExecutor.executeNamedQuery("TelemetryByMeasure", params, pageable);
	}
	@Transactional(value = "BandDataTransactionManager")
	@Override
	public Page<Object> executeUserTelemetry(Pageable pageable, java.lang.String StartDate, java.lang.String EndDate, java.lang.String DeviceId)
	throws QueryParameterMismatchException{
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("StartDate", StartDate);
        params.put("EndDate", EndDate);
        params.put("DeviceId", DeviceId);
        return queryExecutor.executeNamedQuery("UserTelemetry", params, pageable);
	}

	@Transactional(value = "BandDataTransactionManager")
	@Override
	public Page<Object> executeWMCustomQuerySelect(CustomQuery query, Pageable pageable) {
	    return queryExecutor.executeCustomQuery(query, pageable);
	}

	@Transactional(value = "BandDataTransactionManager")
    @Override
    public int executeWMCustomQueryUpdate(CustomQuery query) {
        return queryExecutor.executeCustomQueryForUpdate(query);
    }
}

