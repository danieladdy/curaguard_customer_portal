/*Copyright (c) 2016-2017 curaguard.co.uk All Rights Reserved.
 This software is the confidential and proprietary information of curaguard.co.uk You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with curaguard.co.uk*/

package com.curaguard_customer_portal.banddata.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;

import com.curaguard_customer_portal.banddata.Organisation;

/**
 * Service object for domain model class Organisation.
 *
 * @see {@link Organisation}
 */
public interface OrganisationService {

    /**
     * Creates a new Organisation.
     *
     * @param organisation The information of the created CompositeTable.
     * @return The created Organisation.
     */
	Organisation create(Organisation organisation);


	/**
	 * Finds Organisation by id.
	 *
	 * @param organisationId The id of the wanted Organisation.
	 * @return The found Organisation. If no Organisation is found, this method returns null.
	 */
	Organisation getById(Integer organisationId) throws EntityNotFoundException;

	/**
	 * Updates the information of a Organisation.
	 *
	 * @param organisation The information of the updated Organisation.
	 * @return The updated Organisation.
     *
	 * @throws EntityNotFoundException if no Organisation is found with given id.
	 */
	Organisation update(Organisation organisation) throws EntityNotFoundException;

    /**
	 * Deletes a Organisation.
	 *
	 * @param organisationId The id of the deleted Organisation.
	 * @return The deleted Organisation.
     *
	 * @throws EntityNotFoundException if no Organisation is found with the given id.
	 */
	Organisation delete(Integer organisationId) throws EntityNotFoundException;

	/**
	 * Finds all Organisations.
	 *
	 * @return A list of Organisations.
	 */
    @Deprecated
	Page<Organisation> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Finds all Organisations.
	 * @return A list of Organisations.
	 */
    Page<Organisation> findAll(String query, Pageable pageable);

    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Organisations in the repository with matching query.
     *
     * @param query query to filter results.
	 * @return The count of the Organisation.
	 */
	long count(String query);

    Page<Organisation> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);

}

