/*Copyright (c) 2016-2017 curaguard.co.uk All Rights Reserved.
 This software is the confidential and proprietary information of curaguard.co.uk You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with curaguard.co.uk*/

package com.curaguard_customer_portal.banddata.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.curaguard_customer_portal.banddata.UserAlerts;
import com.curaguard_customer_portal.banddata.UserAlertsId;
import com.curaguard_customer_portal.banddata.service.UserAlertsService;
import com.wordnik.swagger.annotations.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class UserAlerts.
 * @see UserAlerts
 */
@RestController("BandData.UserAlertsController")
@RequestMapping("/BandData/UserAlerts")
@Api(description = "Exposes APIs to work with UserAlerts resource.", value = "UserAlertsController")
public class UserAlertsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAlertsController.class);

    @Autowired
    @Qualifier("BandData.UserAlertsService")
    private UserAlertsService userAlertsService;

    /**
     * @deprecated Use {@link #findUserAlerts(String)} instead.
     */
    @Deprecated
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of UserAlerts instances matching the search criteria.")
    public Page<UserAlerts> findUserAlerts(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering UserAlerts list");
        return userAlertsService.findAll(queryFilters, pageable);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of UserAlerts instances matching the search criteria.")
    public Page<UserAlerts> findUserAlerts(@RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering UserAlerts list");
        return userAlertsService.findAll(query, pageable);
    }

    @RequestMapping(value = "/export/{exportType}", method = RequestMethod.GET, produces = "application/octet-stream")
    @ApiOperation(value = "Returns downloadable file for the data.")
    public Downloadable exportUserAlerts(@PathVariable("exportType") ExportType exportType, @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        return userAlertsService.export(exportType, query, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service UserAlertsService instance
	 */
    protected void setUserAlertsService(UserAlertsService service) {
        this.userAlertsService = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new UserAlerts instance.")
    public UserAlerts createUserAlerts(@RequestBody UserAlerts useralerts) {
        LOGGER.debug("Create UserAlerts with information: {}", useralerts);
        useralerts = userAlertsService.create(useralerts);
        LOGGER.debug("Created UserAlerts with information: {}", useralerts);
        return useralerts;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the UserAlerts instance associated with the given composite-id.")
    public UserAlerts getUserAlerts(@RequestParam(value = "userId", required = true) int userId, @RequestParam(value = "alertId", required = true) int alertId) throws EntityNotFoundException {
        UserAlertsId useralertsId = new UserAlertsId();
        useralertsId.setUserId(userId);
        useralertsId.setAlertId(alertId);
        LOGGER.debug("Getting UserAlerts with id: {}", useralertsId);
        UserAlerts useralerts = userAlertsService.getById(useralertsId);
        LOGGER.debug("UserAlerts details with id: {}", useralerts);
        return useralerts;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Updates the UserAlerts instance associated with the given composite-id.")
    public UserAlerts editUserAlerts(@RequestParam(value = "userId", required = true) int userId, @RequestParam(value = "alertId", required = true) int alertId, @RequestBody UserAlerts useralerts) throws EntityNotFoundException {
        UserAlertsId useralertsId = new UserAlertsId();
        useralertsId.setUserId(userId);
        useralertsId.setAlertId(alertId);
        userAlertsService.delete(useralertsId);
        useralerts = userAlertsService.create(useralerts);
        LOGGER.debug("UserAlerts details with id is updated: {}", useralerts);
        return useralerts;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Deletes the UserAlerts instance associated with the given composite-id.")
    public boolean deleteUserAlerts(@RequestParam(value = "userId", required = true) int userId, @RequestParam(value = "alertId", required = true) int alertId) throws EntityNotFoundException {
        UserAlertsId useralertsId = new UserAlertsId();
        useralertsId.setUserId(userId);
        useralertsId.setAlertId(alertId);
        LOGGER.debug("Deleting UserAlerts with id: {}", useralertsId);
        UserAlerts useralerts = userAlertsService.delete(useralertsId);
        return useralerts != null;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of UserAlerts instances.")
    public Long countUserAlerts(@RequestParam(value = "q", required = false) String query) {
        LOGGER.debug("counting UserAlerts");
        return userAlertsService.count(query);
    }
}
