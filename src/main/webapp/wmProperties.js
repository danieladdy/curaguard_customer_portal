var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "defaultLanguage" : "en",
  "displayName" : "Curaguard Customer Portal",
  "homePage" : "NiceLogin",
  "name" : "Curaguard_Customer_Portal",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};