Application.$controller("UserDashboardPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

        $scope.startyesterday = moment().startOf('day').subtract(1, 'day').format('YYYY-MM-DD');
        $scope.startday = moment().startOf('day').format('YYYY-MM-DD');
        $scope.startweek = moment().startOf('week').format('YYYY-MM-DD');
        $scope.endday = moment().startOf('day').add(1, 'd').format('YYYY-MM-DD');

        $scope.startseven = moment().startOf('day').subtract(7, 'day').format('YYYY-MM-DD');
        $scope.startthirty = moment().startOf('day').subtract(30, 'day').format('YYYY-MM-DD');
        $scope.startninety = moment().startOf('day').subtract(90, 'day').format('YYYY-MM-DD');

    };

}]);